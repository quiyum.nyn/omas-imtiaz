<script>
    var doc = new jsPDF();
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

    $('#cmd').click(function () {
        doc.fromHTML($('#content').html(), 15, 15, {
            'width': 170,
            'elementHandlers': specialElementHandlers
        });
        doc.save('sample-file.pdf');
    });
</script>
<script src="<?php echo base_url?>resources/jsPDF/jspdf.js"></script>
<script src="<?php echo base_url?>resources/jsPDF/from_html.js"></script>
<script src="<?php echo base_url?>resources/jsPDF/split_text_to_size.js"></script>
<script src="<?php echo base_url?>resources/jsPDF/standard_fonts_metrics.js"></script>
