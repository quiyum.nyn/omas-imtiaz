<?php
$data=$auth->showUser();
?>
<header class="main-header" xmlns="http://www.w3.org/1999/html">
    <!-- Logo -->
    <a href="<?php echo base_url?>views/doctor/index.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>D</b>R.</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Doc</b>Tor</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?php echo base_url?>resources/doctor_photos/<?php echo $data->picture?>" class="user-image" alt="User Image">
                    <span class="hidden-xs"><?php echo $data->doctor_name?></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="<?php echo base_url?>resources/doctor_photos/<?php echo $data->picture?>" class="img-circle" alt="User Image">

                        <p>
                            <?php echo $data->doctor_name?> - Member
                            <small>Member since <?php echo date("d-m-Y",strtotime($data->reg_date))?></small>
                        </p>
                    </li>
                    <!-- Menu Body -->
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="doctorProfile.php" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="<?php echo base_url?>controller/doctorLogout.php" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
        </ul>
        </div>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url?>resources/doctor_photos/<?php echo $data->picture?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $data->doctor_name?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active ">
                <a href="<?php echo base_url?>views/doctor/index.php">
                    <i class="fa fa-smile-o "></i> <span>Profile</span>
                </a>
            </li>
            <li class="">
                <a href="<?php echo base_url?>views/doctor/viewappointment.php">
                    <i class="fa fa-sticky-note-o"></i> <span>View Appointment</span>
                </a>
            </li>
            <li class="">
                <a href="<?php echo base_url?>views/doctor/doctor_report.php">
                    <i class="fa fa-sticky-note-o"></i> <span>Patient Report</span>
                </a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
