<div class="header_agileinfo">
    <div class="w3_agileits_header_text">
        <ul class="top_agile_w3l_info_icons">
            <li><i class="fa fa-home" aria-hidden="true"></i></li>
            <li class="second"><i class="fa fa-phone" aria-hidden="true"></i>(+018) 21 542 846 </li>

            <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="#">omas@gmail.com</a></li>
        </ul>

    </div>
    <div class="agileinfo_social_icons">
        <ul class="agileits_social_list">
            <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
            <li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
        </ul>
    </div>
    <div class="clearfix"> </div>
</div>

<div class="header-bottom">
    <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="logo">
                <h1><a class="navbar-brand"  href="<?php echo base_url?>views/index.php"><span>O.</span>M.A.S <p></p></a></h1>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <nav class="menu menu--sebastian">
                <ul id="m_nav_list" class="m_nav menu__list">
                    <li class="m_nav_item menu__item " id="m_nav_item_1"> <a href="<?php echo base_url?>views/index.php" class="menu__link"> Home </a></li>
                    <li class="m_nav_item menu__item" id="moble_nav_item_2"> <a href="<?php echo base_url?>views/hospital.php" class="menu__link">Hospital</a> </li>
                    <li class="m_nav_item menu__item" id="moble_nav_item_2"> <a href="<?php echo base_url?>views/about.php" class="menu__link"> About Us </a> </li>
                    <li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Appointment  <b class="caret"></b></a>
                        <ul class="dropdown-menu agile_short_dropdown">
                            <li><a href="<?php echo base_url?>views/appointment.php">Take Appointment</a></li>
                            <li><a href="<?php echo base_url?>views/searchSlip.php">Search Slip</a></li>
                        </ul>
                    </li>
                    <li class="m_nav_item menu__item" id="moble_nav_item_6"> <a href="<?php echo base_url?>views/contact.php" class="menu__link"> Contact </a> </li>
                    <li class="m_nav_item menu__item" id="moble_nav_item_8"> <a href="<?php echo base_url?>views/emergency.php" class="menu__link"> Emergency </a> </li>
                    <li class="m_nav_item menu__item" id="moble_nav_item_3 dropdown"> <a href="#" class="menu__link dropdown-toggle" data-toggle="dropdown">Access  <b class="caret"></b></a>
                        <ul class="dropdown-menu agile_short_dropdown">
                            <li><a href="<?php echo base_url?>views/registration.php">Registration</a></li>
                            <li><a href="<?php echo base_url?>views/login.php">Login</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>

        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>