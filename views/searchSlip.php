<?php
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
date_default_timezone_set('Asia/Dhaka');
$current_date=date("Y-m-d");
$last_date=date( "Y-m-d", strtotime( "$current_date +2 day" ) );
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <!-- for-meta-tags-->
    <?php require_once ("templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner -->
<!-- banner1 -->
<div class="banner1 jarallax appointment_banner">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.php">Home</a><i>|</i></li>
            <li>Appointment</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->
<!-- icons -->
<div class="banner-bottom" id="about">
    <div class="container">
        <h2 class="w3_heade_tittle_agile">Appointment</h2>
        <p class="sub_t_agileits">If you forgot to download the PDF, You can get it here by giving your Token Number. </p>

        <div class="book-appointment" style="margin-bottom: 30px">
            <h4>Get Your Slip

            </h4>
            <form  method="post" action="appointment-slip.php">
                <div class="row same">
                    <div class="gaps col-md-6 col-md-offset-3">
                        <p>Token ID</p>
                        <input type="text" name="id" placeholder="Input Your Token Number" required=""/>
                    </div>
                <div class="clearfix"></div>
                <input type="submit" value="Search">
            </form>
<br><br>
        </div>
    </div>

</div>

<!-- icons -->




<!-- footer -->
<?php require_once ("templateLayout/footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<?php require_once ("templateLayout/script.php");?>
<!-- //here ends scrolling icon -->
</body>
</html>