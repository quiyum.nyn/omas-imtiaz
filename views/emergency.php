<?php
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
use App\model\Emergency;
$obj=new Emergency();
$allData=$obj->showall();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <!-- for-meta-tags-->
    <?php require_once ("templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner1 -->
<div class="banner1 jarallax emergency_banner">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.php">Home</a><i>|</i></li>
            <li>Emergency</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->

<div class="about" id="about">
    <div class="container">
        <h2 class="w3_heade_tittle_agile">Emergency</h2>
        <p class="sub_t_agileits"></p>

        <p class="ab">This is provided for emergency cases only. For save time in any emergency case it will be helpful.</p>

        <div class="about-w3lsrow">

            <div class="col-md-12">
                <div class="box-body">
                    <form action="../controller/addEmergency.php" method="post">

                        <div class="col-md-12">
                            <table id="example" class="table table-bordered table-striped" style="border: double;margin: auto ;border-color: #0a0a0a;font-family: 'Cambria'; ">
                                <thead style="border-color: #0a0a0a;">
                                <tr style="border-color: #0a0a0a">
                                    <th style="border-color: #0a0a0a">Serial</th>
                                    <th style="border-color: #0a0a0a">Name</th>
                                    <th style="border-color: #0a0a0a">Number</th>
                                    <th style="border-color: #0a0a0a">Type</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Serial</th>
                                    <th>Name</th>
                                    <th>Number</th>
                                    <th>Type</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                <?php
                                $serial= 1;
                                foreach ($allData as $data){
                                    ?>

                                    <tr>
                                        <td><?php echo $serial?></td>
                                        <td><?php echo $data->name?></td>
                                        <td><?php echo $data->phone?></td>
                                        <td><?php echo $data->type?></td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }
                                ?>


                                </tbody>
                            </table>
                        </div>
                        <br><br>

                    </form>

                </div>
            </div>
            <div class="col-md-12">
                <!-- general form elements -->
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>

<!-- footer -->
<?php require_once ("templateLayout/footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- js -->
<?php require_once ("templateLayout/script.php");?>

</body>
</html>