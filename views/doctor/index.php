<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/info.php");
use App\Message\Message;
use App\model\Doctor_master;
$auth= new Doctor_master();
use App\Utility\Utility;
if($_SESSION['role_status']==0){
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
$docObj=new Doctor_master();
$docObj->prepareData($_SESSION);
$oneDoctor=$docObj->showProfile();
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/doctor/css.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php require_once ("../templateLayout/doctor/navigation.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Doctor
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box" style="min-height: 770px">
                        <div class="box-header">
                            <h3>Doctor Profile</h3>
                            <?php
                            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                                $msg = Message::getMessage();

                                echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                            }

                            ?>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="col-md-6 col-md-offset-3" style="border: 2px #387bad solid">
                                <div class="col-md-4 col-md-offset-4 thumbnail">
                                    <img src="../../resources/doctor_photos/<?php echo $oneDoctor->picture?>" class="img-responsive">
                                </div>
                                <div class="col-md-10 col-md-offset-1">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>Name</td>
                                            <td class="text-right"><?php echo $oneDoctor->doctor_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Registration Date</td>
                                            <td class="text-right"><?php echo $oneDoctor->reg_date?></td>
                                        </tr>
                                        <tr>
                                            <td>Gender</td>
                                            <td class="text-right"><?php echo $oneDoctor->gender?></td>
                                        </tr>
                                        <tr>
                                            <td>Contact</td>
                                            <td class="text-right"><?php echo $oneDoctor->contact?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td class="text-right"><?php echo $oneDoctor->email?></td>
                                        </tr>
                                        <tr>
                                            <td>Degree</td>
                                            <td class="text-right"><?php echo $oneDoctor->degree?></td>
                                        </tr>
                                        <tr>
                                            <td>Hospital</td>
                                            <td class="text-right"><?php echo $oneDoctor->hospital_name?></td>
                                        </tr>
                                        <tr>
                                            <td>Category</td>
                                            <td class="text-right"><?php echo $oneDoctor->category_name?></td>
                                        </tr>

                                        <tr>
                                            <td>Patient Limit</td>
                                            <td class="text-right"><?php echo $oneDoctor->patient_limit?></td>
                                        </tr>
                                        <tr>
                                            <td>Fees</td>
                                            <td class="text-right"><?php echo $oneDoctor->fees?></td>
                                        </tr>
                                        <tr>
                                            <td>Times</td>
                                            <td class="text-right"><?php echo $oneDoctor->time?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">You Can De-active your account!</button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">De-active your account!</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure to de-active your account!? </p>
                                                    <a href="../../controller/deactiveDoctor.php?id=<?php echo $oneDoctor->doctor_master_id?>" class="btn btn-danger">Yes</a>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <br><br>
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php require_once ("../templateLayout/doctor/footer.php");?>
</div>
<!-- ./wrapper -->

<?php require_once ("../templateLayout/doctor/script.php");?>
</body>
</html>
