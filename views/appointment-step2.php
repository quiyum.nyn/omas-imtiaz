<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
use App\Message\Message;
use App\model\Patients;
$serialObj=new Patients();
$serialObj->prepareData($_POST);
$serial=$serialObj->serial();
$limit=$_POST['limit'];

if(isset($serial->serial)){
    if($serial->serial<$limit){
        $set='1';
    }
    else{
        $set='0';
    }

}
else {
    $set='1';

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <!-- for-meta-tags-->
    <?php require_once ("templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner -->
<!-- banner1 -->
<div class="banner1 jarallax appointment_banner">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.php">Home</a><i>|</i></li>
            <li>Appointment</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->
<!-- icons -->
<div class="banner-bottom" id="about">
    <div class="container">
        <h2 class="w3_heade_tittle_agile">Appointment</h2>
        <p class="sub_t_agileits">By giving information's here anyone can fix an appointment with a doctor. No one need to register for use this service but must need to provide Name and Telephone number & also Download the PDF file that contains a Token Number.</p>

        <div class="book-appointment">
            <h4>Make an appointment
                <?php
                if(isset($_SESSION) && !empty($_SESSION['message'])) {

                    $msg = Message::getMessage();

                    echo "<p class='help-block' style='color: #ff0000;text-align: center'>$msg</p>";
                }

                ?>
            </h4>
            <?php
                if($set==1){
                   ?>
                    <form  method="post" action="../controller/takeAppointment.php" enctype="multipart/form-data">
                        <div class="row same">
                            <div class="gaps col-md-6">
                                <p>Patient Name</p>
                                <input type="text" name="p_name" placeholder="Patient Name" required=""/>
                            </div>
                            <div class="gaps col-md-6">
                                <p>Phone Number</p>
                                <input type="number" name="p_contact" placeholder="Patient Contact" required=""/>
                            </div>

                            <div class="gaps col-md-6">
                                <p>Age</p>
                                <input type="number" name="p_age" placeholder=" Patient Age" required=""/>
                            </div>
                            <div class="gaps col-md-6">
                                <p>Email</p>
                                <input type="email" name="p_email" placeholder="Patient Email" required/>
                            </div>
                            <div class="gaps col-md-6">
                                <p>Gender</p>
                                <select class="option" name="p_gender">
                                    <option value="unspecified">select gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>

                            <div class="gaps  col-md-6">
                                <p>Choose a picture</p>
                                <input type="file" name="picture"  accept="image/*" required=""/>
                            </div>
                        </div>
                        <input type="hidden" name="p_date" value="<?php echo $_POST['b_date']?>" >
                        <input type="hidden" name="time" value="<?php echo $_POST['time']?>" >
                        <input type="hidden" name="doctor_master_id" value="<?php echo $_POST['doctor_master_id']?>">
                        <input type="hidden" name="hospital_master_id" value="<?php echo $_POST['hospital_master_id']?>">
                        <input type="hidden" name="category_id" value="<?php echo $_POST['category_id']?>">
                        <div class="clearfix"></div>
                        <input type="submit" value="Appoint">
                    </form>

                    <?php
                }
            else{
                ?>
                <h3 style="text-align: center;color: red"> Queue is full.</h3>
                <?php
                    if($serial->serial==1){
                        ?>
                        <h3 style="text-align: center;color: red"> All ready <?php echo $serial->serial?> patients takes his appointment.</h3>
                        <?php
                    }
                else{
                    ?>
                    <h3 style="text-align: center;color: red"> All ready <?php echo $serial->serial?> patients takes their appointment.</h3>
                    <?php
                }
                ?>

                <h3 style="text-align: center;color: #005e00"> Daily patient limit <?php echo $limit?></h3>

                <form action="appointment.php" method="post">
                    <input type="hidden" name="category_id" value="<?php echo $_POST['category_id']?>">
                    <input type="hidden" name="appoint_days" value="<?php echo $_POST['appoint_days']?>">
                    <input type="hidden" name="a_date" value="<?php echo $_POST['b_date']?>">
                    <input type="hidden" name="hospital_id_new" value="<?php echo $_POST['hospital_master_id']?>">
                    <input type="submit" value="Appoint another doctor" class="btn btn-primary">
                </form>

                <?php
            }
            ?>

        </div>
    </div>

</div>

<!-- icons -->




<!-- footer -->
<?php require_once ("templateLayout/footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<?php require_once ("templateLayout/script.php");?>
<!-- //here ends scrolling icon -->
</body>
</html>