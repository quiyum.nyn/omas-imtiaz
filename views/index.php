<?php
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $title?></title>
<!-- for-meta-tags-->
<?php require_once ("templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner -->
<div class="banner-silder">
    <div id="JiSlider" class="jislider">
        <ul>
            <li>
                <div class="w3layouts-banner-top">

                    <div class="container">
                        <div class="agileits-banner-info">
                            <span>OMAS</span>
                            <h3>Quality Care </h3>
                            <p>We try our best to give the best service and care.</p>

                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="w3layouts-banner-top w3layouts-banner-top1">
                    <div class="container">
                        <div class="agileits-banner-info">
                            <span>Time Saver</span>
                            <h3>Fix Appointment </h3>
                            <p>It's easy to fix an appoinment by using OMAS.</p>

                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="w3layouts-banner-top w3layouts-banner-top2">
                    <div class="container">
                        <div class="agileits-banner-info">
                            <span>Danger</span>
                            <h3>Emergency </h3>
                            <p>In case of any emergency we've provided numbers of doctor's & Ambulence Service's.</p>

                        </div>

                    </div>
                </div>
            </li>

        </ul>
    </div>
</div>

<!-- //banner -->

<!-- about -->
<div class="about" id="about">
    <div class="container">
        <h2 class="w3_heade_tittle_agile">Welcome to OMAS</h2>
        <p class="sub_t_agileits">What is OMAS!!!</p>

        <p class="ab">OMAS means Online Medical Appointment System which provides you previlages of fixing appointment with a doctor by staying at home or your office. It will save your time and reduce the chance of being frusrated by not getting the serial number.</p>

        <div class="about-w3lsrow">

            <div class="col-md-6 w3about-img">
                <img src="<?php echo base_url?>resources/user/images/about.jpg" alt=" " class="img-responsive">
            </div>
            <div class="col-md-6 col-sm-7 w3about-img two">
                <div class="w3about-text">
                    <h5 class="w3l-subtitle">We Care About You</h5>
                    <p>The main moto of OMAS is to serve the people, to bring changes into the relationship between the doctor and the patient.</p>

                    <p>Using this may b reduce the corruption of changing serial number,wasting of precious time.</p>
                    <div class="read"><a href="#" class="hvr-rectangle-in">Read More</a></div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- /about-bottom -->
<!-- /girds_agileits -->
<div class="Department_girds_agileits">
    <div class="agile_team_grid">

        <div class="col-md-3 w3ls_banner_bottom_grid">
            <img src="<?php echo base_url?>resources/user/images/g1.jpg" alt=" " class="img-responsive" />
            <div class="overlay">
                <h4>Neurology</h4>
                <ul class="social_agileinfo">
                    <li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" class="w3_google"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-3 w3ls_banner_bottom_grid">
            <img src="<?php echo base_url?>resources/user/images/g2.jpg" alt=" " class="img-responsive" />
            <div class="overlay">
                <h4>Baby Care</h4>
                <ul class="social_agileinfo">
                    <li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" class="w3_google"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>


        <div class="col-md-3 w3ls_banner_bottom_grid hvr-shutter-in-horizontal">
            <img src="<?php echo base_url?>resources/user/images/g3.jpg" alt=" " class="img-responsive" />
            <div class="overlay">
                <h4>Eye care</h4>
                <ul class="social_agileinfo">
                    <li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" class="w3_google"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>


        <div class="col-md-3 w3ls_banner_bottom_grid">
            <img src="<?php echo base_url?>resources/user/images/g4.jpg" alt=" " class="img-responsive" />
            <div class="overlay">
                <h4>Dental</h4>
                <ul class="social_agileinfo">
                    <li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#" class="w3_google"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
</div>
<!-- //girds_agileits -->

<div class="agile_menu" id="menu">
    <div class="container">
        <h3 class="w3_heade_tittle_agile">Categories</h3>
        <p class="sub_t_agileits"></p>
        <div class="menu_w3ls_agile_top_section">
            <div class="ziehharmonika">
                <h3>Neurology</h3>
                <div>
                    <div class="w3_agile_recipe-grid">

                        <div class="col-md-6 col-sm-6 tab-image">
                            <img src="<?php echo base_url?>resources/user/images/g5.jpg" alt="Medicinal">
                        </div>
                        <div class="col-md-6 col-sm-6 tab-info">
                            <h4>We are having department to us</h4>
                            <p>The branch of medicine or biology that deals with the anatomy, functions, and organic disorders of nerves and the nervous system.A neurologist is a physician specializing in neurology and trained to investigate, or diagnose and treat neurological disorders.</p>
                            <div class="read"><a href="#" class="hvr-rectangle-in">Read More</a></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
                <h3>Dental</h3>
                <div>
                    <div class="w3_agile_recipe-grid">


                        <div class="col-md-6 col-sm-6 tab-info two">
                            <h4>We are having department to us</h4>
                            <p>Dental health is another way of saying oral health or the health of your mouth. The mouth is a window into the health of your body. </p>
                            <div class="read"><a href="#" class="hvr-rectangle-in">Read More</a></div>
                        </div>
                        <div class="col-md-6 col-sm-6 tab-image two">
                            <img src="<?php echo base_url?>resources/user/images/g6.jpg" alt="Medicinal">
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
                <h3>Eye Care</h3>
                <div>
                    <div class="w3_agile_recipe-grid">

                        <div class="col-md-6 col-sm-6 tab-image">
                            <img src="<?php echo base_url?>resources/user/images/g7.jpg" alt="Medicinal">
                        </div>
                        <div class="col-md-6 col-sm-6 tab-info">
                            <h4>We are having department to us</h4>
                            <p> It is actually a sofisticated matter to handle, we do our best to offer to the patient. </p>
                            <div class="read"><a href="#" class="hvr-rectangle-in">Read More</a></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>


                </div>
                <h3>Baby Care</h3>
                <div>
                    <div class="w3_agile_recipe-grid">


                        <div class="col-md-6 col-sm-6 tab-info two">
                            <h4>We are having department to us</h4>
                            <p>Baby care is a unit where babies get best care not equal to the parents care of course but we try our best to give. </p>
                            <div class="read"><a href="#" class="hvr-rectangle-in">Read More</a></div>
                        </div>
                        <div class="col-md-6 col-sm-6 tab-image two">
                            <img src="<?php echo base_url?>resources/user/images/g2.jpg" alt="Medicinal">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- services section -->
<div class="service-w3l jarallax" id="service">
    <div class="container">
        <h3 class="w3_heade_tittle_agile two">What We Do Best</h3>
        <p class="sub_t_agileits"></p>
        <div class="col-lg-4 col-md-4 col-sm-12 serv-agileinfo1">
            <div class="col-lg-12 col-md-12 col-sm-6 serv-wthree1" data-aos="zoom-in">
                <ul class="ch-grid">
                    <li>
                        <div class="ch-item">
                            <div class="ch-info">
                                <div class="ch-info-front ch-img-1"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
                                <div class="ch-info-back">
                                    <h5>OMAS</h5>
                                    <p>Best In Services</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <h4 class="text-center">Appointment</h4>
                <p class="text-center">Easy to fix appointment with the doctor.</p>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-6 serv-wthree2" data-aos="zoom-in">
                <ul class="ch-grid">
                    <li>
                        <div class="ch-item">
                            <div class="ch-info">
                                <div class="ch-info-front ch-img-2"><i class="fa fa-user-md" aria-hidden="true"></i></div>
                                <div class="ch-info-back">
                                    <h5>OMAS</h5>
                                    <p>Best In Services</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <h4 class="text-center">Experience</h4>
                <p class="text-center">We try to take out the best doctor's.</p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 serv-agileinfo2">
            <div class="col-lg-12 col-md-12 col-sm-6 serv-wthree4" data-aos="zoom-in">
                <ul class="ch-grid">
                    <li>
                        <div class="ch-item">
                            <div class="ch-info">
                                <div class="ch-info-front ch-img-3"><i class="fa fa-ambulance" aria-hidden="true"></i></div>
                                <div class="ch-info-back">
                                    <h5>OMAS</h5>
                                    <p>Best In Services</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <h4 class="text-center">Emergency Service</h4>
                <p class="text-center">We also provide emergency service.</p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 serv-agileinfo3">
            <div class="col-lg-12 col-md-12 col-sm-6 serv-wthree6" data-aos="zoom-in">
                <ul class="ch-grid">
                    <li>
                        <div class="ch-item">
                            <div class="ch-info">
                                <div class="ch-info-front ch-img-4"><i class="fa fa-tint" aria-hidden="true"></i></div>
                                <div class="ch-info-back">
                                    <h5>OMAS</h5>
                                    <p>Best In Services</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <h4 class="text-center">Emergency Service</h4>
                <p class="text-center">We also provide emergency service.</p>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-6 serv-wthree5" data-aos="zoom-in">
                <ul class="ch-grid">
                    <li>
                        <div class="ch-item">
                            <div class="ch-info">
                                <div class="ch-info-front ch-img-5"><i class="fa fa-wheelchair" aria-hidden="true"></i></div>
                                <div class="ch-info-back">
                                    <h5>OMAS</h5>
                                    <p>Best In Services</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <h4 class="text-center">General Treatment</h4>
                <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla volutpat risus eu pretium commodo.</p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- /services section -->

<!-- stats -->
<div class="stats" id="stats">
    <div class="container">
        <div class="inner_w3l_agile_grids">
            <div class="col-md-3 w3layouts_stats_left w3_counter_grid">
                <i class="fa fa-building-o" aria-hidden="true"></i>
                <p class="counter">4</p>
                <h3>Hospitals</h3>
            </div>
            <div class="col-md-3 w3layouts_stats_left w3_counter_grid1">
                <i class="fa fa-user" aria-hidden="true"></i>
                <p class="counter">60</p>
                <h3>Doctors</h3>
            </div>
            <div class="col-md-3 w3layouts_stats_left w3_counter_grid1">
                <i class="fa fa-ambulance" aria-hidden="true"></i>
                <p class="counter">8</p>
                <h3>Ambulence</h3>
            </div>


            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //stats -->

<!-- services-bottom -->
<div class="services-bottom">
    <div class="col-md-5 wthree_services_bottom_right">
        <section class="slider">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <div class="w3_agile_services_bottom_right_grid">
                            <p class="w3layouts_head_slide">Health Care <span>Center</span></p>
                        </div>
                    </li>
                    <li>
                        <div class="w3_agile_services_bottom_right_grid1">
                            <p class="w3layouts_head_slide">Hospital <span> Departments</span></p>
                        </div>
                    </li>
                    <li>
                        <div class="w3_agile_services_bottom_right_grid2">
                            <p class="w3layouts_head_slide">Health Care <span>Center</span></p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </div>
    <div class="col-md-7 wthree_services_bottom_left">
        <div class="wthree_services_bottom_left_grid">
            <div class="col-md-6 w3_agileits_services_bottom_l_grid">
                <div class="agile_services_bottom_l_grid1">
                    <img src="<?php echo base_url?>resources/user/images/g4.jpg" alt=" " class="img-responsive" />
                    <div class="w3_service_bottom_grid_pos">
                        <h3>Quality Care</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-6 w3_agileits_services_bottom_r_grid">
                <h4>Best Surgeons</h4>
                <p>We do try to give access to the best surgeons.</p>

            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="wthree_services_bottom_left_grid">
            <div class="col-md-6 w3_agileits_services_bottom_r_grid">
                <h4>Latest Technologies</h4>
                <p> This hospital does have the best equipment in the city.
                    Also the got CCU,ICU etc.</p>

            </div>
            <div class="col-md-6 w3_agileits_services_bottom_l_grid">
                <div class="agile_services_bottom_l_grid1">
                    <img src="<?php echo base_url?>resources/user/images/g2.jpg" alt=" " class="img-responsive" />
                    <div class="w3_service_bottom_grid_pos">
                        <h3>Quality Care</h3>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="clearfix"> </div>
</div>
<!-- //services-bottom -->

<!-- footer -->
<?php require_once ("templateLayout/footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- js -->


<?php require_once ("templateLayout/script.php");?>
</body>
</html>