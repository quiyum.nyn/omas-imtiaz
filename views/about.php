<?php
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <!-- for-meta-tags-->
    <?php require_once ("templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner -->
<!-- banner1 -->
<div class="banner1 about_banner jarallax">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.php">Home</a><i>|</i></li>
            <li>About</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->


<!-- //banner -->

<!-- about -->
<div class="about" id="about">
    <div class="container">
        <h2 class="w3_heade_tittle_agile">Welcome to OMAS</h2>
        <p class="sub_t_agileits"></p>

        <p class="ab">This is developed for fullfilling some crysis that usually occurs when we go to fix appointment with a doctor. We have to wait a long time or else we don't get one. For resolving that kind of problem here's OMAS></p>


        <!-- team -->
        <div class="team portfolio " id="team">
            <div class="container">
                <h3 class="w3_heade_tittle_agile">Hospitals</h3>
                <p class="sub_t_agileits">Hospital those use our system...</p>
                <div class="w3layouts-grids">
                    <div class="col-md-3 wthree_team_grid">
                        <div class="wthree_team_grid1">
                            <div class="hover14 column">
                                <div>
                                    <figure><img src="<?php echo base_url?>resources/user/images/t4.jpg" alt=" " class="img-responsive" /></figure>
                                </div>
                            </div>
                            <div class="wthree_team_grid1_pos">
                                <h4> Chittagong Metropolitan Hospital</h4>
                            </div>
                        </div>
                        <div class="wthree_team_grid2">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 wthree_team_grid">
                        <div class="wthree_team_grid1">
                            <div class="hover14 column">
                                <div>
                                    <figure><img src="<?php echo base_url?>resources/user/images/t3.jpg" alt=" " class="img-responsive" /></figure>
                                </div>
                            </div>
                            <div class="wthree_team_grid1_pos">
                                <h4>CSCR </h4>
                            </div>
                        </div>
                        <div class="wthree_team_grid2">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 wthree_team_grid">
                        <div class="wthree_team_grid1">
                            <div class="hover14 column">
                                <div>
                                    <figure><img src="<?php echo base_url?>resources/user/images/t2.jpg" alt=" " class="img-responsive" /></figure>
                                </div>
                            </div>
                            <div class="wthree_team_grid1_pos">
                                <h4>MAX</h4>
                            </div>
                        </div>
                        <div class="wthree_team_grid2">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 wthree_team_grid">
                        <div class="wthree_team_grid1">
                            <div class="hover14 column">
                                <div>
                                    <figure><img src="<?php echo base_url?>resources/user/images/t1.jpg" alt=" " class="img-responsive" /></figure>
                                </div>
                            </div>
                            <div class="wthree_team_grid1_pos">
                                <h4>National Hospital Ltd</h4>
                            </div>
                        </div>
                        <div class="wthree_team_grid2">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!-- //team -->
        <!-- stats -->
        <div class="stats_inner jarallax" id="stats">
            <div class="container">
                <div class="inner_w3l_agile_grids">
                    <div class="col-md-3 w3layouts_stats_left w3_counter_grid">
                        <i class="fa fa-building-o" aria-hidden="true"></i>
                        <p class="counter">4</p>
                        <h3>Hospitals</h3>
                    </div>
                    <div class="col-md-3 w3layouts_stats_left w3_counter_grid1">
                        <i class="fa fa-wheelchair" aria-hidden="true"></i>
                        <p class="counter">60</p>
                        <h3>Doctors</h3>
                    </div>
                    <div class="col-md-3 w3layouts_stats_left w3_counter_grid2">
                        <i class="fa fa-ambulance" aria-hidden="true"></i>
                        <p class="counter">8</p>
                        <h3>Ambulance</h3>
                    </div>

                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div>

        <!-- //stats -->

        <!-- footer -->
        <?php require_once ("templateLayout/footer.php");?>
        <!-- //footer -->
        <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
        <!-- js -->

        <?php require_once ("templateLayout/script.php");?>
</body>
</html>