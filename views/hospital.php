<?php
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner -->
<!-- banner1 -->
<div class="banner1 about_banner jarallax">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.php">Home</a><i>|</i></li>
            <li>Hospital</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->


<!-- //banner -->

<!-- about -->
<div class="about" id="about">
    <div class="container">
        <h2 class="w3_heade_tittle_agile"></h2>
        <p class="sub_t_agileits"></p>

        <p class="ab">Patient will get here detail information about any hospital those  uses our system.</p>


        <!-- team -->
        <div class="team portfolio " id="team">
            <div class="container">
                <h3 class="w3_heade_tittle_agile">Hospitals</h3>
                <p class="sub_t_agileits">Hospital those use our system...</p>
                <div class="w3layouts-grids">
                    <div class="col-md-3 wthree_team_grid">
                        <div class="wthree_team_grid1">
                            <div class="hover14 column">
                                <div>
                                    <figure><a href="hospital/metropoliton.php"><img src="<?php echo base_url?>resources/user/images/t4.jpg" alt=" " class="img-responsive" /></figure>

                                </div>
                            </div>
                            <div class="wthree_team_grid1_pos">
                                <h4><a href="hospital/metropoliton.php"> Chittagong Metropolitan Hospital</a></h4>
                            </div>
                        </div>
                        <div class="wthree_team_grid2">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 wthree_team_grid">
                        <div class="wthree_team_grid1">
                            <div class="hover14 column">
                                <div>
                                    <figure><a href="hospital/cscr.php"><img src="<?php echo base_url?>resources/user/images/t3.jpg" alt=" " class="img-responsive" /></figure>
                                </div>
                            </div>
                            <div class="wthree_team_grid1_pos">
                                <h4><a href="hospital/cscr.php"> CSCR </a></h4>
                            </div>
                        </div>
                        <div class="wthree_team_grid2">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 wthree_team_grid">
                        <div class="wthree_team_grid1">
                            <div class="hover14 column">
                                <div>
                                    <figure><a href="hospital/max.php"><img src="<?php echo base_url?>resources/user/images/t2.jpg" alt=" " class="img-responsive" /></figure>
                                </div>
                            </div>
                            <div class="wthree_team_grid1_pos">
                                <h4><a href="hospital/max.php"> MAX </a></h4>
                            </div>
                        </div>
                        <div class="wthree_team_grid2">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 wthree_team_grid">
                        <div class="wthree_team_grid1">
                            <div class="hover14 column">
                                <div>
                                    <figure><a href="hospital/national.php"><img src="<?php echo base_url?>resources/user/images/t1.jpg" alt=" " class="img-responsive" /></figure>
                                </div>
                            </div>
                            <div class="wthree_team_grid1_pos">
                                <h4><a href="hospital/national.php"> National Hospital Ltd</a></h4>
                            </div>
                        </div>
                        <div class="wthree_team_grid2">
                            <ul class="social-icons">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!-- //team -->
        <!-- stats -->
        <div class="stats_inner jarallax" id="stats">
            <div class="container">
                <div class="inner_w3l_agile_grids">
                    <div class="col-md-3 w3layouts_stats_left w3_counter_grid">
                        <i class="fa fa-building-o" aria-hidden="true"></i>
                        <p class="counter">4</p>
                        <h3>Hospitals</h3>
                    </div>
                    <div class="col-md-3 w3layouts_stats_left w3_counter_grid1">
                        <i class="fa fa-wheelchair" aria-hidden="true"></i>
                        <p class="counter">60</p>
                        <h3>Doctors</h3>
                    </div>
                    <div class="col-md-3 w3layouts_stats_left w3_counter_grid2">
                        <i class="fa fa-ambulance" aria-hidden="true"></i>
                        <p class="counter">8</p>
                        <h3>Ambulance</h3>
                    </div>

                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <!-- //stats -->

        <!-- footer -->
        <!-- footer -->
        <?php require_once ("templateLayout/footer.php");?>
        <!-- //footer -->
        <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
        <!-- js -->
        <?php require_once ("templateLayout/script.php");?>
</body>
</html>