<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/info.php");
use App\Message\Message;
use App\model\Super_admins;
$auth= new Super_admins();
use App\Utility\Utility;
if($_SESSION['role_status']==1){
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Hospital_master;
$hosObj=new Hospital_master();
$hosObj->prepareData($_GET);
$oneData=$hosObj->showone();
$allData=$hosObj->showCategorynDoctor();
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/admin/css.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php require_once ("../templateLayout/admin/navigation.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Hospital List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Hospital's</a></li>
                <li class="active">Home</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3>Hospital Details</h3>
                            <?php
                            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                                $msg = Message::getMessage();

                                echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                            }

                            ?>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <div>
                                <table class="table table-striped">
                                    <tr>
                                        <td>Hospital Name</td>
                                        <td><?php echo $oneData->hospital_name?></td>
                                    </tr>
                                    <tr>
                                        <td>Location</td>
                                        <td><?php echo $oneData->location?></td>
                                    </tr>
                                    <tr>
                                        <td>Contact</td>
                                        <td><?php echo $oneData->contact?></td>
                                    </tr>
                                </table>
                            </div>
                            <h3>Doctor List</h3>
                            <table id="example" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>Doctor Name</th>
                                    <th>Contact</th>
                                    <th>Degree</th>
                                    <th>Category</th>

                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Serial</th>
                                    <th>Doctor Name</th>
                                    <th>Contact</th>
                                    <th>Degree</th>
                                    <th>Category</th>
                                </tr>
                                </tfoot>
                                <tbody>

                                <?php
                                $serial= 1;
                                foreach ($allData as $data){
                                    ?>

                                    <tr>
                                        <td><?php echo $serial?></td>
                                        <td><?php echo $data->doctor_name?></td>
                                        <td><?php echo $data->contact?></td>
                                        <td><?php echo $data->degree?></td>
                                        <td><?php echo $data->category_name?></td>
                                    </tr>
                                    <?php
                                    $serial++;
                                }
                                ?>


                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <?php require_once ("../templateLayout/admin/footer.php");?>
</div>

<?php require_once ("../templateLayout/admin/script.php");?>
</body>
</html>
