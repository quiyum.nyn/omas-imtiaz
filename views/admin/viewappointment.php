<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/info.php");
use App\Message\Message;
use App\model\Super_admins;
$auth= new Super_admins();
use App\Utility\Utility;
if($_SESSION['role_status']==1){
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/admin/css.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php require_once ("../templateLayout/admin/navigation.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List</h3>

                    <div class="box-tools">
                        <div style="width: 150px;" class="input-group input-group-sm">
                            <input type="text" placeholder="Search" class="form-control pull-right" name="table_search">

                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody><tr>
                            <th>ID</th>
                            <th>Patient Name</th>
                            <th>Age</th>
                            <th>Phone Number</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Mohammad Hasan</td>
                            <td>24</td>
                            <td>018xxxxxx</td>
                            <td>Hasan@gmail.com</td>
                            <td>Male</td>
                            <td>25/07/2017</td>
                            <td>10.00am</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Mohammad Irfan</td>
                            <td>24</td>
                            <td>018xxxxxx</td>
                            <td>irfan@gmail.com</td>
                            <td>Male</td>
                            <td>25/07/2017</td>
                            <td>10.30am</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Miss Neela</td>
                            <td>22</td>
                            <td>018xxxxxx</td>
                            <td>neela@gmail.com</td>
                            <td>female</td>
                            <td>25/07/2017</td>
                            <td>11.00am</td>
                        </tr>
                        </tbody></table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->
    </div>
        <?php require_once ("../templateLayout/admin/footer.php");?>
    </div>

    <?php require_once ("../templateLayout/admin/script.php");?>
</body>
</html>
