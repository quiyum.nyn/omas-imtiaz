<?php
session_start();
    require_once ("../../vendor/autoload.php");
    require_once ("../templateLayout/info.php");
use App\Message\Message;
use App\model\Super_admins;
$auth= new Super_admins();
use App\Utility\Utility;
if($_SESSION['role_status']==1){
  $status = $auth->prepareData($_SESSION)->logged_in();

  if(!$status) {
    Utility::redirect('../login.php');
    Message::setMessage("Please LogIn first");
    return;
  }
}
else {
  Message::setMessage("Please LogIn first");
  Utility::redirect('../login.php');
}
$auth->prepareData($_SESSION);
$adminData=$auth->showProfile();
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title?></title>
  <?php require_once ("../templateLayout/admin/css.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php require_once ("../templateLayout/admin/navigation.php");?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box" style="min-height: 770px">
            <div class="box-header">
              <h3>Admin Profile</h3>
              <?php
              if(isset($_SESSION) && !empty($_SESSION['message'])) {

                $msg = Message::getMessage();

                echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
              }

              ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="col-md-6 col-md-offset-3" style="border: 2px #387bad solid">
                <div class="col-md-4 col-md-offset-4 thumbnail">
                  <img src="../../resources/admin_photos/<?php echo $adminData->admin_picture?>" class="img-responsive">
                </div>
                <div class="col-md-10 col-md-offset-1">
                  <table class="table ">
                    <tbody>
                    <tr>
                      <td>Name</td>
                      <td class="text-right"><?php echo $adminData->admin_name?></td>
                    </tr>
                    <tr>
                      <td>Contact</td>
                      <td class="text-right"><?php echo $adminData->admin_contact?></td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td class="text-right"><?php echo $adminData->admin_email?></td>
                    </tr>

                    </tbody>
                  </table>
                </div>
              </div>


            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <?php require_once ("../templateLayout/admin/footer.php");?>
</div>
<!-- ./wrapper -->

<?php require_once ("../templateLayout/admin/script.php");?>
</body>
</html>
