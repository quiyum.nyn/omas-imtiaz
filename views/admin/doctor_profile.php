<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/info.php");
use App\Message\Message;
use App\model\Super_admins;
$auth= new Super_admins();
use App\Utility\Utility;
if($_SESSION['role_status']==1){
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Doctor_master;
$docObj=new Doctor_master();
$docObj->prepareData($_GET);
$oneDoctor=$docObj->showOne();
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/admin/css.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php require_once ("../templateLayout/admin/navigation.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Admin Panel
                <small>Doctor List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Hospital's</a></li>
                <li class="active">Home</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box" style="min-height: 770px">
                        <div class="box-header">
                            <h3>Doctor Profile</h3>
                            <?php
                            if(isset($_SESSION) && !empty($_SESSION['message'])) {

                                $msg = Message::getMessage();

                                echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                            }

                            ?>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="col-md-6 col-md-offset-3" style="border: 2px #387bad solid">
                                    <div class="col-md-4 col-md-offset-4 thumbnail">
                                        <img src="../../resources/doctor_photos/<?php echo $oneDoctor->picture?>" class="img-responsive">
                                    </div>
                              <div class="col-md-10 col-md-offset-1">
                                  <table class="table table-bordered">
                                      <tbody>
                                      <tr>
                                          <td>Name</td>
                                          <td class="text-right"><?php echo $oneDoctor->doctor_name?></td>
                                      </tr>
                                      <tr>
                                          <td>Registration Date</td>
                                          <td class="text-right"><?php echo $oneDoctor->reg_date?></td>
                                      </tr>
                                      <tr>
                                          <td>Gender</td>
                                          <td class="text-right"><?php echo $oneDoctor->gender?></td>
                                      </tr>
                                      <tr>
                                          <td>Contact</td>
                                          <td class="text-right"><?php echo $oneDoctor->contact?></td>
                                      </tr>
                                      <tr>
                                          <td>Email</td>
                                          <td class="text-right"><?php echo $oneDoctor->email?></td>
                                      </tr>
                                      <tr>
                                          <td>Degree</td>
                                          <td class="text-right"><?php echo $oneDoctor->degree?></td>
                                      </tr>
                                      <tr>
                                          <td>Hospital</td>
                                          <td class="text-right"><?php echo $oneDoctor->hospital_name?></td>
                                      </tr>
                                      <tr>
                                          <td>Category</td>
                                          <td class="text-right"><?php echo $oneDoctor->category_name?></td>
                                      </tr>

                                      <tr>
                                          <td>Patient Limit</td>
                                          <td class="text-right"><?php echo $oneDoctor->patient_limit?></td>
                                      </tr>
                                      <tr>
                                          <td>Fees</td>
                                          <td class="text-right"><?php echo $oneDoctor->fees?></td>
                                      </tr>
                                      <tr>
                                          <td>Times</td>
                                          <td class="text-right"><?php echo $oneDoctor->time?></td>
                                      </tr>
                                      </tbody>
                                  </table>
                                  <?php
                                    if($oneDoctor->status=='1'){
                                       ?>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Block this doctor's activities</button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Block all activities</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure to block this doctor's activities? </p>
                                                        <a href="../../controller/blockDoctor.php?id=<?php echo $oneDoctor->doctor_master_id?>" class="btn btn-danger">Yes</a>
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                  <?php
                                    }
                                  else{
                                     ?>
                                      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Active this doctor's activities</button>
                                      <!-- Modal -->
                                      <div class="modal fade" id="myModal" role="dialog">
                                          <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                  <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title">Active all activities</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                      <p>Are you sure to active this doctor's activities? </p>
                                                      <a href="../../controller/activeDoctor.php?id=<?php echo $oneDoctor->doctor_master_id?>" class="btn btn-primary">Yes</a>
                                                      <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                                                  </div>
                                                  <div class="modal-footer">
                                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  <?php
                                  }
                                  ?>
                                  <br><br>
                              </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <?php require_once ("../templateLayout/admin/footer.php");?>
</div>

<?php require_once ("../templateLayout/admin/script.php");?>
</body>
</html>
