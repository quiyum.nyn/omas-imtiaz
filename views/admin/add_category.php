<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/info.php");
use App\Message\Message;
use App\model\Super_admins;
$auth= new Super_admins();
use App\Utility\Utility;
if($_SESSION['role_status']==1){
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Category;
$obj=new Category();
$allData=$obj->showall();

?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/admin/css.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php require_once ("../templateLayout/admin/navigation.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Category
                <small>panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Category</a></li>
                <li class="active">Home</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Enter Category</h3>
                        </div>
                        <?php
                        if(isset($_SESSION) && !empty($_SESSION['message'])) {

                            $msg = Message::getMessage();

                            echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                        }

                        ?>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="post" action="../../controller/addCategory.php">
                            <div class="box-body">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="form-group ">
                                        <label for="exampleInputEmail1">Category Name</label>
                                        <input type="text" placeholder="Enter Category Name" name="cat_name" id="exampleInputEmail1" class="form-control">
                                    </div>
                                    <button class="btn btn-primary" name="rsubmit" type="submit">Submit</button>
                                </div>

                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Category Lookup</h3>
                        </div>
                        <div class="box-body">
                            <form action="../../controller/addHospital.php" method="post">

                                <div class="col-md-12">
                                    <table id="example" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Serial</th>
                                            <th>Category Name</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>Serial</th>
                                            <th>Category Name</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>

                                        <?php
                                        $serial= 1;
                                        foreach ($allData as $data){
                                            ?>

                                            <tr>
                                                <td><?php echo $serial?></td>
                                                <td><?php echo $data->category_name?></td>
                                                <td style="width: 5%"><a href='editCategory.php?id=<?php echo $data->id?>' class='btn btn-primary'><i class='fa fa-external-link-square' aria-hidden='true'></i></a></td>
                                                <td style="width: 5%"><a href='../../controller/deleteCategory.php?id=<?php echo $data->id?>' class='btn btn-danger'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>
                                            </tr>
                                            <?php
                                            $serial++;
                                        }
                                        ?>


                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" name="hos_name" value="<?php echo $allTempMasterData->hospital_name?>">
                                <input type="hidden" name="location" value="<?php echo $allTempMasterData->location?>">
                                <input type="hidden" name="contact" value="<?php echo $allTempMasterData->contact?>">
                                <?php
                                if($status){
                                    ?>
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal2" style="margin-left: 50px">Submit Information and store</button>
                                            <div class="modal fade" id="myModal2" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title"><?php echo $allTempMasterData->hospital_name?></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4>Are You Sure to store this hospital details?</h4>
                                                            <button class="btn btn-info" type="submit">Yes</button>
                                                            <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>


                                <br><br>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <?php require_once ("../templateLayout/admin/footer.php");?>
</div>

<?php require_once ("../templateLayout/admin/script.php");?>
</body>
</html>
