<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/info.php");
use App\Message\Message;
use App\model\Super_admins;
$auth= new Super_admins();
use App\Utility\Utility;
if($_SESSION['role_status']==1){
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}

?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/admin/css.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php require_once ("../templateLayout/admin/navigation.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Doctor Details</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="">
                    <div class="box-body">
                        <div class="form-group ">
                            <label for="exampleInputEmail1">Doctor Name</label>
                            <input type="text" placeholder="Enter Doctor Full Name" name="doc_name" id="exampleInputEmail1" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Phone Number</label>
                            <input type="text" placeholder="Enter Doctor Phone Number" name="doc_num" id="exampleInputEmail1" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" placeholder="Enter Doctor Email Address" name="doc_email" id="exampleInputEmail1" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Hospital Name</label>
                            <select class="form-control select2" name="hos_nam" style="width: 100%;">
                                <option selected="#"></option>
                                <option>Chittagong Metropoliton Hospital</option>
                                <option>MAX Hospital</option>
                                <option>Royal Hospital</option>
                                <option>National Hospital Limited</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Category</label>
                            <label>Category</label>
                            <select class="form-control select2" name="doc_cat" style="width: 100%;">
                                <option selected="#"></option>
                                <option>Cardiology</option>
                                <option>Ophthalmology</option>
                                <option>Neurology</option>
                                <option>Psychology</option>
                                <option>Dermatology</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Gender</label>
                            <label>Gender</label>
                            <select class="form-control select2" name="doc_gen" style="width: 100%;">
                                <option selected="#"></option>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Degree</label>
                            <label>Degree</label>
                            <select class="form-control select2" name="doc_deg" style="width: 100%;">
                                <option selected="#"></option>
                                <option>F.C.F.S</option>
                                <option>M.B.B.S</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Password</label>
                            <input type="password" placeholder="Enter Password" name="doc_pass" id="exampleInputEmail1" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Confirm Password</label>
                            <input type="password" placeholder="Enter Password Again" name="doc_pasc" id="exampleInputEmail1" class="form-control">
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button class="btn btn-primary" name="rsubmit" type="submit">Submit</button>
                    </div>
                </form>
            </div>

</section>
<!-- /.content -->
    </div>
    <?php require_once ("../templateLayout/admin/footer.php");?>
</div>

<?php require_once ("../templateLayout/admin/script.php");?>
</body>
</html>
