<?php
session_start();
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/info.php");
use App\Message\Message;
use App\model\Super_admins;
$auth= new Super_admins();
use App\Utility\Utility;
if($_SESSION['role_status']==1){
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        Message::setMessage("Please LogIn first");
        return;
    }
}
else {
    Message::setMessage("Please LogIn first");
    Utility::redirect('../login.php');
}
use App\model\Category;
use App\model\Temp;
$tempObj=new Temp();
$catObj=new Category();
$allCategory=$catObj->showall();
$allTempData=$tempObj->showall();
$status=$tempObj->is_exist();
$allTempMasterData=$tempObj->showMaster();
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/admin/css.php");?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php require_once ("../templateLayout/admin/navigation.php");?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Hospital panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Hospital</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Enter Hospital Details <?php if($status){ echo "<button type=\"button\" class=\"btn btn-danger\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"margin-left: 50px\">Cancel to add this hospital details</button>";}?></h3>
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><?php echo $allTempMasterData->hospital_name?></h4>
                                </div>
                                <div class="modal-body">
                                    <h4>Are You Sure to delete this list?</h4>
                                    <a href="../../controller/deleteTemp.php" class="btn btn-primary">Yes</a>
                                    <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <?php
                if(isset($_SESSION) && !empty($_SESSION['message'])) {

                    $msg = Message::getMessage();

                    echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
                }

                ?>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="../../controller/tempHospitalData.php">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="exampleInputEmail1">Hospital Name</label>
                                    <input type="text" placeholder="Enter Hospital Name" name="hos_name" id="exampleInputEmail1" value="<?php if($status){echo $allTempMasterData->hospital_name;}?>" class="form-control" <?php if($status){echo "readonly";}?>>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Hospital address</label>
                                    <input type="text" placeholder="Enter Hospital Address" name="hos_add" value="<?php if($status){echo $allTempMasterData->location;}?>" id="exampleInputEmail1" class="form-control" <?php if($status){echo "readonly";}?>>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Contact No</label>
                                    <input type="number" placeholder="Enter Hospital Contact Number" name="hos_con" value="<?php if($status){echo $allTempMasterData->contact;}?>" id="exampleInputEmail1" class="form-control" <?php if($status){echo "readonly";}?>>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Add category</label>
                                    <select name="category_id" class="form-control">
                                        <option value="reject">-select category-</option>
                                        <?php
                                            foreach ($allCategory as $cat){
                                                ?>
                                                <option value="<?php echo $cat->id?>"><?php echo $cat->category_name?></option>
                                        <?php
                                            }
                                        ?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-6">
                                <button class="btn btn-primary" type="submit">Add Another Category</button>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">

                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Hospital Lookup</h3>
                </div>
                <div class="box-body">
                   <form action="../../controller/addHospital.php" method="post">

                       <div class="col-md-12">
                           <table id="example" class="table table-bordered table-striped">
                               <thead>
                               <tr>
                                   <th>Serial</th>
                                   <th>Category Name</th>
                                   <th>Delete</th>
                               </tr>
                               </thead>
                               <tfoot>
                               <tr>
                                   <th>Serial</th>
                                   <th>Category Name</th>
                                   <th>Delete</th>
                               </tr>
                               </tfoot>
                               <tbody>

                               <?php
                               $serial= 1;
                               foreach ($allTempData as $data){
                                   ?>

                                   <tr>
                                       <td><?php echo $serial?></td>
                                       <td><input type="text" class="form-control" style="width: 100%" value="<?php echo $data->category_name?>" readonly> <input type="hidden" name="cat_id[]" value="<?php echo $data->category_id?>"></td>
                                       <td style="width: 5%"><a href='../../controller/deleteTempOne.php?id=<?php echo $data->id?>' class='btn btn-danger'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>
                                   </tr>
                                   <?php
                                   $serial++;
                               }
                               ?>


                               </tbody>
                           </table>
                       </div>
                       <input type="hidden" name="hos_name" value="<?php echo $allTempMasterData->hospital_name?>">
                       <input type="hidden" name="location" value="<?php echo $allTempMasterData->location?>">
                       <input type="hidden" name="contact" value="<?php echo $allTempMasterData->contact?>">
                       <?php
                        if($status){
                            ?>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal2" style="margin-left: 50px">Submit Information and store</button>
                                    <div class="modal fade" id="myModal2" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"><?php echo $allTempMasterData->hospital_name?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <h4>Are You Sure to store this hospital details?</h4>
                                                    <button class="btn btn-info" type="submit">Yes</button>
                                                    <a href="" data-dismiss="modal" class="btn btn-danger">No</a>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                       <?php
                        }
                       ?>


                       <br><br>

                   </form>

                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
    </div>
    <?php require_once ("../templateLayout/admin/footer.php");?>
</div>

<?php require_once ("../templateLayout/admin/script.php");?>
</body>
</html>
