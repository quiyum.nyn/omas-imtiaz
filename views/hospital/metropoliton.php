<?php
require_once ("../../vendor/autoload.php");
require_once ("../templateLayout/info.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <?php require_once ("../templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("../templateLayout/navigation.php");?>
</div>
<!-- banner -->
<!-- banner1 -->
<div class="banner1 about_banner jarallax">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="../hospital.php">Back</a><i>|</i></li>
            <li>Metropoliton Hospital LTD</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->


<!-- //banner -->

<!-- about -->
<div class="about" id="about">
    <div class="container">
        <h2 class="w3_heade_tittle_agile"></h2>
        <p class="sub_t_agileits"></p>

        <p class="ab">Patient will get here detail information about doctor's of Metropoliton Hospital LTD!</p>
    </div>
</div>
<div class="map_agile">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d29521.610102309925!2d91.80477929941407!3d22.346028189410404!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1499804809514" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<?php require_once ("../templateLayout//footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- js -->
<?php require_once ("../templateLayout/script.php");?>
</body>
</html>