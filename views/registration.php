<?php
session_start();
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
use App\Message\Message;
use App\model\Hospital_master;
use App\model\Hospital_details;
use App\model\Doctor_master;
$doctorMasterObj=new Doctor_master();
$detailsObj=new Hospital_details();
$hospitalObj=new Hospital_master();
$allHospitalData=$hospitalObj->showall();
if(isset($_POST['hospital_id'])){
   $detailsObj->prepareData($_POST);
    $allCat=$detailsObj->showCategory();
    $doctorMasterObj->prepareData($_POST);
    $status=$doctorMasterObj->is_exist_email();
}
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title><?php echo $title?></title>
    <?php require_once ("templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner1 -->
<div class="banner1 jarallax">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.php">Home</a><i>|</i></li>
            <li>Registration</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->

<div class="banner-bottom" id="about">
    <div class="container">
        <?php
        if(isset($_SESSION) && !empty($_SESSION['message'])) {

            $msg = Message::getMessage();

            echo "<p class='help-block' style='color: #0c5577;text-align: center'>$msg</p>";
        }

        ?>
        <h2 class="w3_heade_tittle_agile">Registration</h2>
        <p class="sub_t_agileits">This is for those doctor whom are interested in using this system to serve.</p>

        <div class="book-appointment">
            <h4>Registration
                <?php
                if (isset($report)){
                    if ($report>0){
                        echo '<small> <i class="fa fa-check text-success"></i> Successful</small>';
                    }
                }

                ?>
            </h4>

            <?php
                if(isset($_POST['hospital_id'])){
                    if($_POST['hospital_id']=='reject'){
                        ?>
                        <h3 style="text-align: center;color: red">Error!Please select a hospital</h3>
                        <center><a href="registration.php" class="btn btn-primary">Back</a></center>
                        <?php
                    }
                    else{
                        if($status){
                            ?>
                            <h3 style="text-align: center;color: red">Error!This email is already taken!</h3>
                            <center><a href="registration.php" class="btn btn-primary">Back</a></center>
                            <?php
                        }
                        else{
                            if($_POST['pass']==$_POST['cpass']){
                                ?>
                                <form action="registration.php" method="post" enctype="multipart/form-data">
                                    <div class="row same">
                                        <div class="gaps  col-md-6 col-md-offset-3">
                                            <p>Category</p>
                                            <select class="option" name="category_id">
                                                <option value="reject">select a category</option>
                                                <?php
                                                foreach ($allCat as $data){
                                                    ?>
                                                    <option value="<?php echo $data->category_id?>"><?php echo $data->cat_name?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <input type="hidden" name="name" value="<?php echo $_POST['name']?>"/>
                                        <input type="hidden" name="contact" value="<?php echo $_POST['contact']?>"/>
                                        <input type="hidden" name="email" value="<?php echo $_POST['email']?>"/>
                                        <input type="hidden" name="hospital_id_new" value="<?php echo $_POST['hospital_id']?>" />
                                        <input type="hidden" name="degree" value="<?php echo $_POST['degree']?>"/>
                                        <input type="hidden" name="gender" value="<?php echo $_POST['gender']?>"/>
                                        <input type="hidden" name="pass" value="<?php echo $_POST['pass']?>"/>
                                        <input type="hidden" name="cpass" value="<?php echo $_POST['cpass']?>"/>

                                        <div class="gaps  col-md-6 col-md-offset-3">
                                            <input type="submit"  value="Next Step">
                                        </div>
                                    </div>
                                </form>
                                <?php
                            }
                            else{
                                ?>
                                <h3 style="text-align: center;color: red">Error!Confirm password doesn't match</h3>
                                <center><a href="registration.php" class="btn btn-primary">Back</a></center>
                                <?php
                            }
                        }
                    }

                }
            else if(isset($_POST['category_id'])){
                if($_POST['category_id']=='reject'){
                   ?>
                    <h3 style="text-align: center;color: red">Error!Please select a cetegory</h3>
                    <center><a href="registration.php" class="btn btn-primary">Back</a></center>
                    <?php
                }
                else{
                   ?>
                    <form action="../controller/registration.php" method="post" enctype="multipart/form-data">
                        <div class="row same">
                            <div class="gaps  col-md-6">
                                <p>Patients Limit</p>
                                <input type="number" name="patient_limit" placeholder="Set daily patient  limit" required=""/>
                            </div>
                            <div class="gaps  col-md-6">
                                <p>Fees</p>
                                <input type="number" name="fee" placeholder="Fee" required=""/>
                            </div>
                            <div class="gaps  col-md-6">
                                <p>Choose a picture</p>
                                <input type="file" name="picture_data"  accept="image/*" required=""/>
                            </div>
                            <div class="gaps  col-md-6">
                                <p>Time</p>
                                <input type="text" name="time"  placeholder="Time (ex. 10:00am to 01:00pm)" required=""/>
                            </div>
                            <div class="gaps  col-md-6" style="color: white">
                                <p>Days of activities</p>
                                <input type="checkbox" name="days[]" value="Sat" > Sat
                                <input type="checkbox" name="days[]" value="Sun" > Sun
                                <input type="checkbox" name="days[]" value="Mon" > Mon
                                <input type="checkbox" name="days[]" value="Tue" > Tue
                                <input type="checkbox" name="days[]" value="Wed" > Wed
                                <input type="checkbox" name="days[]" value="Thu" > Thu
                                <input type="checkbox" name="days[]" value="Fri" > Fri
                            </div>
                            <input type="hidden" name="name" value="<?php echo $_POST['name']?>"/>
                            <input type="hidden" name="contact" value="<?php echo $_POST['contact']?>"/>
                            <input type="hidden" name="email" value="<?php echo $_POST['email']?>"/>
                            <input type="hidden" name="hospital_id_new" value="<?php echo $_POST['hospital_id_new']?>" />
                            <input type="hidden" name="degree" value="<?php echo $_POST['degree']?>"/>
                            <input type="hidden" name="gender" value="<?php echo $_POST['gender']?>"/>
                            <input type="hidden" name="pass" value="<?php echo $_POST['pass']?>"/>
                            <input type="hidden" name="category_id" value="<?php echo $_POST['category_id']?>"/>
                            <div class="gaps  col-md-6 col-md-offset-3">
                                <input type="submit"  value="Registration">
                            </div>

                        </div>
                    </form>
                    <?php
                }

            }
            else{
                ?>
                <form action="registration.php" method="post" enctype="multipart/form-data">
                    <div class="row same">
                        <div class="gaps col-md-12">
                            <p>Doctor's Name</p>
                            <input type="text" name="name" placeholder="Doctor Name" />
                        </div>
                        <div class="gaps  col-md-6">
                            <p>Phone Number</p>
                            <input type="number" name="contact" placeholder="Phone Number" required=""/>
                        </div>
                        <div class="gaps  col-md-6">
                            <p>Email</p>
                            <input type="email" name="email" placeholder="Email" required="" />
                        </div>
                        <div class="gaps  col-md-6">
                            <p>Hospital</p>
                            <select class="option" name="hospital_id">
                                <option value="reject">select a medical</option>
                                <?php
                                foreach ($allHospitalData as $data){
                                    ?>
                                    <option value="<?php echo $data->id?>"><?php echo $data->hospital_name?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="gaps  col-md-6">
                            <p>Degree</p>
                            <input type="text" name="degree" placeholder="Enter your degree" required=""/>
                        </div>

                        <div class="gaps  col-md-6">
                            <p>Gender</p>
                            <select class="option" name="gender">
                                <option value="reject">select your gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="gaps  col-md-6">
                            <p>Password</p>
                            <input type="password" name="pass" placeholder="Enter password with atleast 6 Characters" minlength="6" required="" />
                        </div>
                        <div class="gaps  col-md-6">
                            <p>Confirm Password</p>
                            <input type="password" name="cpass" placeholder="Enter password with atleast 6 Characters" minlength="6" required="" />
                        </div>
                        <div class="gaps  col-md-6 col-md-offset-3">
                            <input type="submit"  value="Next Step">
                        </div>


                    </div>
                </form>
            <?php
            }
            ?>

        </div>
    </div>
</div>

<!-- footer -->
<?php require_once ("templateLayout/footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- js -->
<?php require_once ("templateLayout/script.php");?>
</body>
</html>