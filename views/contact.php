<?php
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <!-- for-meta-tags-->
    <?php require_once ("templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner -->
<!-- banner1 -->
<div class="banner1 jarallax contact_banner">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.php">Home</a><i>|</i></li>
            <li>Contact</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->
<div class="banner-bottom" id="about">
    <div class="container">
        <h2 class="w3_heade_tittle_agile">Contact Us</h2>
        <p class="sub_t_agileits">Get in touch...</p>

        <div class="contact-top-agileits">
            <div class="col-md-4 contact-grid ">
                <div class="contact-grid1 agileits-w3layouts">
                    <i class="fa fa-location-arrow"></i>
                    <div class="con-w3l-info">
                        <h4>Address</h4>
                        <p><span>Chittagong</span></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-4 contact-grid">
                <div class="contact-grid2 w3">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <div class="con-w3l-info">
                        <h4>Call Us</h4>
                        <p><span>+018 21 542 846</span></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-4 contact-grid">
                <div class="contact-grid3 w3l">
                    <i class="fa fa-envelope"></i>
                    <div class="con-w3l-info">
                        <h4>Email</h4>
                        <p><a href="mailto:info@example.com">omas@gmail.com</a>

                        </p></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>

        </div>
        <div class="contact-form-aits">
            <form action="#" method="post">
                <input type="text" class="margin-right" name="Name" placeholder="Name" required="">
                <input type="email" name="Email" placeholder="Email" required="">
                <input type="text" class="margin-right" name="Phone Number" placeholder="Phone Number" required="">
                <input type="text" name="Subject" placeholder="Subject" required="">
                <textarea name="Message" placeholder="Message" required=""></textarea>
                <div class="send-button agileits w3layouts">
                    <button class="btn btn-primary">SEND MESSAGE</button>
                </div>
            </form>
            <ul class="agileits_social_list two">
                <li class="fallow"> Follow Us :</li>
                <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                <li><a href="#" class="w3_agile_rss"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
            </ul>

        </div>
    </div>
</div>

<div class="map_agile">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d29521.610102309925!2d91.80477929941407!3d22.346028189410404!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1499804809514" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- footer -->
<?php require_once ("templateLayout/footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- js -->
<?php require_once ("templateLayout/script.php");?>
</body>
</html>