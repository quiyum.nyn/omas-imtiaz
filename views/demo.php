<?php
require_once ("../vendor/autoload.php");
require_once ("templateLayout/info.php");
date_default_timezone_set('Asia/Dhaka');
$current_date=date("Y-m-d");
$last_date=date( "Y-m-d", strtotime( "$current_date +2 day" ) );
use App\model\Hospital_details;
use App\model\Hospital_master;
$detailsObj=new Hospital_details();
$hospitalObj=new Hospital_master();
$allHospitalData=$hospitalObj->showall();
if(isset($_POST['hospital_id'])){
    $detailsObj->prepareData($_POST);
    $hospitalObj->prepareData($_POST);
    $oneHospital=$hospitalObj->showone();
    $allCat=$detailsObj->showCategory();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $title?></title>
    <!-- for-meta-tags-->
    <?php require_once ("templateLayout/css.php");?>
</head>

<body>
<div class="main" id="home">
    <!-- banner -->
    <?php require_once ("templateLayout/navigation.php");?>
</div>
<!-- banner -->
<!-- banner1 -->
<div class="banner1 jarallax appointment_banner">
    <div class="container">
    </div>
</div>

<div class="services-breadcrumb">
    <div class="container">
        <ul>
            <li><a href="index.php">Home</a><i>|</i></li>
            <li>Appointment</li>
        </ul>
    </div>
</div>
<!-- //banner1 -->
<!-- icons -->
<div class="banner-bottom" id="about">
    <div class="container">
        <h2 class="w3_heade_tittle_agile">Appointment</h2>
        <p class="sub_t_agileits">By giving informations here anyone can fix an appointment with a doctor. No one need to register for use this service but must need to provide Name and Telephone number & also take the print out of the serial number.</p>

        <div class="book-appointment">
            <h4>Make an appointment

            </h4>
            <?php

            if(isset($_POST['hospital_id'])){
                ?>
                <form  method="post" action="appointment.php">
                    <div class="row same">
                        <div class="gaps col-md-6 col-md-offset-3">
                            <table style="color: white">
                                <tbody>

                                <tr>
                                    <td>Your chosen hospital</td>
                                    <td>:</td>
                                    <td style="color: #13c4c0"><?php echo $oneHospital->hospital_name?></td>
                                </tr>
                                <tr>
                                    <td>Hospital Location</td>
                                    <td>:</td>
                                    <td style="color: #13c4c0"><?php echo $oneHospital->location?></td>
                                </tr>
                                <tr>
                                    <td>Hospital Contact</td>
                                    <td>:</td>
                                    <td style="color: #13c4c0"><?php echo $oneHospital->contact?></td>
                                </tr>
                                </tbody>
                            </table>
                            <br>
                            <p>Category</p>
                            <select class="option" name="category_id">
                                <option value="reject">select a category</option>
                                <?php
                                foreach ($allCat as $data){
                                    ?>
                                    <option value="<?php echo $data->category_id?>"><?php echo $data->cat_name?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <input type="text" name="p_name" value="<?php echo $_POST['p_name']?>"/>
                    <input type="number" name="p_contact" value="<?php echo $_POST['p_contact']?>"/>
                    <input type="number" name="p_age" value="<?php echo $_POST['p_age']?>"/>
                    <input type="email" name="p_email" value="<?php echo $_POST['p_email']?>"/>
                    <input type="text" name="p_gender" value="<?php echo $_POST['p_gender']?>"/>
                    <input type="text" name="hospital_new_id" value="<?php echo $_POST['hospital_id']?>"/>
                    <input type="password" name="pass" value="<?php echo $_POST['pass']?>"/>
                    <input type="password" name="cpass" value="<?php echo $_POST['cpass']?>"/>
                    <div class="clearfix"></div>
                    <input type="submit" value="Next Step">
                </form>
                <?php
            }
            else if(isset($_POST['category_id'])){
                ?>
                <form  method="post" action="../controller/takeAppointment.php">
                    <div class="row same">
                        <div class="gaps col-md-6 col-md-offset-3">
                            <p>Doctor</p>
                            <select class="option" name="pdoctor">
                                <option></option>
                            </select>
                        </div>
                        <div class="gaps col-md-6">
                            <p>Select Date</p>
                            <input type="date" name="pdate" min="<?php echo $current_date?>" max="<?php echo $last_date?>" required="">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <input type="submit" name="rsubmit" value="Make an Appointment">
                </form>
                <?php
            }
            else{
                ?>
                <form  method="post" action="appointment.php">
                    <div class="row same">
                        <div class="gaps col-md-6">
                            <p>Patient Name</p>
                            <input type="text" name="p_name" placeholder="Patient Name" required=""/>
                        </div>
                        <div class="gaps col-md-6">
                            <p>Phone Number</p>
                            <input type="number" name="p_contact" placeholder="Patient Contact" required=""/>
                        </div>

                        <div class="gaps col-md-6">
                            <p>Age</p>
                            <input type="number" name="p_age" placeholder=" Patient Age" required=""/>
                        </div>
                        <div class="gaps col-md-6">
                            <p>Email</p>
                            <input type="email" name="p_email" placeholder="Patient Email" required=""/>
                        </div>
                        <div class="gaps col-md-6">
                            <p>Gender</p>
                            <select class="option" name="p_gender">
                                <option value="reject">select gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="gaps col-md-6">
                            <p>Hospital</p>
                            <select class="option" name="hospital_id">
                                <option value="reject">select a hospital</option>
                                <?php
                                foreach ($allHospitalData as $data){
                                    ?>
                                    <option value="<?php echo $data->id?>"><?php echo $data->hospital_name?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="gaps col-md-6">
                            <p>Password</p>
                            <input type="password" name="pass" placeholder="Password ( Al least 6 characters)" minlength="6" required=""/>
                        </div>
                        <div class="gaps col-md-6">
                            <p>Confirm Password</p>
                            <input type="password" name="cpass" placeholder="Confirm Password ( Al least 6 characters)" minlength="6" required=""/>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <input type="submit" value="Next Step">
                </form>
                <?php
            }
            ?>

        </div>
    </div>

</div>

<!-- icons -->




<!-- footer -->
<?php require_once ("templateLayout/footer.php");?>
<!-- //footer -->
<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<?php require_once ("templateLayout/script.php");?>
<!-- //here ends scrolling icon -->
</body>
</html>