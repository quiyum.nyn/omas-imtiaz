<?php
namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Hospital_details extends Database
{

    public $id;
    public $master_id;
    public $category_id;
    public $hospital_master_id;


    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('category_id', $data)) {
            $this->category_id = $data['category_id'];
        }
        if (array_key_exists('hospital_id', $data)) {
            $this->hospital_master_id = $data['hospital_id'];
        }

        return $this;

    }

    public function store(){
        $select_master_id="SELECT id FROM `hospital_master` order by id DESC Limit 1";
        $STH = $this->DBH->query($select_master_id);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->master_id= $row['id'];
        $cat_id=explode(",",$this->category_id);
        $count=0;
        foreach ($cat_id as $cat){
            $query= "INSERT INTO `hospital_details`(master_id,category_id) VALUES (?,?)";
            $STH = $this->DBH->prepare($query);
            $STH->bindParam(1,$this->master_id);
            $STH->bindParam(2,$cat_id[$count]);
            $STH->execute();
            $count++;
        }

    }
    public function showCategory(){
        $sql = "SELECT hospital_details.*, category.category_name as cat_name FROM `hospital_details`,category WHERE `master_id`='$this->hospital_master_id' AND category.id=hospital_details.category_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

}