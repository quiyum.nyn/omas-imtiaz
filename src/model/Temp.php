<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 8/11/2017
 * Time: 2:00 AM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Temp extends Database
{
    public $id;
    public $hospital_name;
    public $location;
    public $contact;
    public $cat_id;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('hos_name', $data)) {
            $this->hospital_name = $data['hos_name'];
        }
        if (array_key_exists('hos_add', $data)) {
            $this->location = $data['hos_add'];
        }
        if (array_key_exists('hos_con', $data)) {
            $this->contact = $data['hos_con'];
        }
        if (array_key_exists('category_id', $data)) {
            $this->cat_id = $data['category_id'];
        }

        return $this;

    }
    public function store(){
        $query= "INSERT INTO `temp`(hospital_name,location,contact,category_id) VALUES (?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->hospital_name);
        $STH->bindParam(2,$this->location);
        $STH->bindParam(3,$this->contact);
        $STH->bindParam(4,$this->cat_id);

        $result = $STH->execute();
        if($result){

            Message::setMessage("Success! Data Insertion successfully Completed!");
        }
        else{
            Message::setMessage("Failed! data has not be inserted!");
        }
    }
    public function showall(){
        $sql = "SELECT temp.*,category.category_name FROM `temp`,category WHERE temp.category_id=category.id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showMaster(){
        $sql = "SELECT * FROM `temp` GROUP BY `hospital_name`";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function is_exist(){

        $query="SELECT * FROM `temp`";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function is_exist_category(){

        $query="SELECT * FROM `temp` WHERE category_id='$this->cat_id'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function delete(){
        $query = "DELETE FROM `temp`";
        $this->DBH->exec($query);
    }

    public function deleteOne(){
        $query = "DELETE FROM `temp` WHERE id='$this->id'";
        $this->DBH->exec($query);
    }

}