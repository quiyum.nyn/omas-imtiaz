<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 8/10/2017
 * Time: 11:51 PM
 */

namespace App\model;

if(!isset($_SESSION) )  session_start();
use App\database\Database;
use PDO;
use App\Message\Message;


class Emergency extends Database
{
    public $id;
    public $name;
    public $phone;
    public $type;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->name = $data['id'];
        }
        if (array_key_exists('emer', $data)) {
            $this->name = $data['emer'];
        }
        if (array_key_exists('emer_no', $data)) {
            $this->phone = $data['emer_no'];
        }
        if (array_key_exists('type', $data)) {
            $this->type = $data['type'];
        }
        return $this;

    }

    public function store(){
        $query= "INSERT INTO `emergency`( `name`, `phone`, `type`) VALUES (?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->name);
        $STH->bindParam(2,$this->phone);
        $STH->bindParam(3,$this->type);

        $result = $STH->execute();
        if($result){

            Message::setMessage("Success! Data Insertion successfully Completed!");
        }
        else{
            Message::setMessage("Failed! data has not be inserted!");
        }
    }
    public function showall(){
        $sql = "SELECT * FROM `emergency` WHERE status='1' ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function is_exist_category(){

        $query="SELECT * FROM `emergency` WHERE name='$this->name'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function deleteOne(){
        $query = "DELETE FROM `emergency` WHERE id='$this->id'";
        $this->DBH->exec($query);
    }
}