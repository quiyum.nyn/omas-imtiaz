<?php
/**
 * Created by PhpStorm.
 * User: kalponik
 * Date: 8/5/2017
 * Time: 8:54 PM
 */

namespace App\model;

if(!isset($_SESSION) )  session_start();
use App\database\Database;
use PDO;
use App\Message\Message;
use App\Utility\Utility;

class Patients extends Database
{
    public $id;
    public $patient_name;
    public $contact;
    public $gender;
    public $age;
    public $email;
    public $password;
    public $hospital_id;
    public $category_id;
    public $doctor_id;
    public $booking_date;
    public $appoint_date;
    public $serial;
    public $picture;
    public $b_date;
    public $time;
    public $doctor_id2;
    public $current_date;

    public function __construct(){
        parent::__construct();
    }
    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('p_name', $data)) {
            $this->patient_name = $data['p_name'];
        }
        if (array_key_exists('p_contact', $data)) {
            $this->contact = $data['p_contact'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('p_age', $data)) {
            $this->age = $data['p_age'];
        }
        if (array_key_exists('p_email', $data)) {
            $this->email = $data['p_email'];
        }
        if (array_key_exists('p_gender', $data)) {
            $this->gender = $data['p_gender'];
        }
        if (array_key_exists('p_date', $data)) {
            $this->appoint_date = $data['p_date'];
        }
        if (array_key_exists('doctor_master_id', $data)) {
            $this->doctor_id = $data['doctor_master_id'];
        }
        if (array_key_exists('hospital_master_id', $data)) {
            $this->hospital_id = $data['hospital_master_id'];
        }
        if (array_key_exists('category_id', $data)) {
            $this->category_id = $data['category_id'];
        }
        if (array_key_exists('picture_name', $data)) {
            $this->picture = $data['picture_name'];
        }
        if (array_key_exists('b_date', $data)) {
            $this->b_date = $data['b_date'];
        }
        if (array_key_exists('time', $data)) {
            $this->time = $data['time'];
        }
        if (array_key_exists('doctor_id', $data)) {
            $this->doctor_id2 = $data['doctor_id'];
        }
        if (array_key_exists('current_day', $data)) {
            $this->current_date = $data['current_day'];
        }
        return $this;
    }
    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->booking_date=$date;
        $query= "INSERT INTO `patient`(patient_name,contact,gender,age,email,password,hospital_id,category_id,doctor_id,booking_date,appoint_date,picture,time) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->patient_name);
        $STH->bindParam(2,$this->contact);
        $STH->bindParam(3,$this->gender);
        $STH->bindParam(4,$this->age);
        $STH->bindParam(5,$this->email);
        $STH->bindParam(6,$this->password);
        $STH->bindParam(7,$this->hospital_id);
        $STH->bindParam(8,$this->category_id);
        $STH->bindParam(9,$this->doctor_id);
        $STH->bindParam(10,$this->booking_date);
        $STH->bindParam(11,$this->appoint_date);
        $STH->bindParam(12,$this->picture);
        $STH->bindParam(13,$this->time);
        $STH->execute();

        $serial="SELECT COUNT(`id`) as id FROM `patient` WHERE `hospital_id`='$this->hospital_id' AND `category_id`='$this->category_id' AND `doctor_id`='$this->doctor_id' AND appoint_date='$this->appoint_date'";
        $STH = $this->DBH->query($serial);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->serial= $row['id'];

        $select_master_id="SELECT id as pat_id FROM `patient` order by id DESC Limit 1";
        $STH = $this->DBH->query($select_master_id);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->id= $row['pat_id'];

        $query2= "UPDATE patient SET serial =? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query2);
        $STH->bindParam(1,$this->serial);
        $STH->execute();
        Message::setMessage("Success! Appointment has been successfully done!");
        Utility::redirect('../views/appointment-slip.php?id='.$this->id);
    }
    public function showone(){
        $sql = "SELECT patient.*,doctor_master.doctor_name,doctor_master.contact as d_contact,doctor_master.degree, hospital_master.hospital_name,hospital_master.location,hospital_master.contact as h_contact,category.category_name FROM `patient`,doctor_master,hospital_master,category WHERE doctor_master.id=patient.doctor_id AND hospital_master.id=patient.hospital_id AND category.id=patient.category_id AND patient.id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function limit(){
        $sql = "SELECT count(id) as count FROM `patient` WHERE appoint_date='$this->appoint_date' AND doctor_id=$this->doctor_id";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function serial(){
        $sql = "SELECT `serial` FROM `patient` WHERE `hospital_id`='$this->hospital_id' AND `category_id`='$this->category_id' AND `doctor_id`='$this->doctor_id' AND appoint_date='$this->b_date' ORDER BY id DESC LIMIT 1";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function exist_token(){
        $sql = "SELECT patient.* FROM `patient` WHERE patient.id='$this->id'";
        $STH=$this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetch();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function showAppoint(){
        $sql = "SELECT patient.*,hospital_master.hospital_name,category.category_name FROM `patient`,hospital_master,category WHERE patient.hospital_id=hospital_master.id AND category.id=patient.category_id AND appoint_date='$this->current_date' AND patient.doctor_id='$this->doctor_id2' ORDER BY patient.serial";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    
}