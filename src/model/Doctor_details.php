<?php
/**
 * Created by PhpStorm.
 * User: kalponik
 * Date: 8/5/2017
 * Time: 8:57 PM
 */

namespace App\model;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class Doctor_details extends Database
{
    public $id;
    public $cat_id;
    public $hospital_master_id;
    public $patient_limit;
    public $days;
    public $fees;
    public $doctor_master_id;
    public $doc_id;
    public $hos_id;
    public $appoint_day;
    public $time;


    public function __construct(){
        parent::__construct();
    }
    public function prepareData($data){
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('category_id', $data)) {
            $this->cat_id = $data['category_id'];
        }
        if (array_key_exists('fee', $data)) {
            $this->fees = $data['fee'];
        }
        if (array_key_exists('patient_limit', $data)) {
            $this->patient_limit = $data['patient_limit'];
        }
        if (array_key_exists('days_details', $data)) {
            $this->days = $data['days_details'];
        }
        if (array_key_exists('hospital_id_new', $data)) {
            $this->hospital_master_id = $data['hospital_id_new'];
        }
        if (array_key_exists('doctor_master_id', $data)) {
            $this->doc_id = $data['doctor_master_id'];
        }
        if (array_key_exists('hospital_master_id', $data)) {
            $this->hos_id = $data['hospital_master_id'];
        }
        if (array_key_exists('appoint_days', $data)) {
            $this->appoint_day = $data['appoint_days'];
        }
        if (array_key_exists('time', $data)) {
            $this->time = $data['time'];
        }


        return $this;
    }
    public function store(){
        $select_master_id="SELECT id FROM `doctor_master` order by id DESC Limit 1";
        $STH = $this->DBH->query($select_master_id);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->doctor_master_id= $row['id'];

        $days=explode(",",$this->days);
        $count=0;
        foreach($days as $pr){
            $query= "INSERT INTO `doctor_details`(category_id,doctor_master_id,hospital_id,patient_limit,days,fees,time) VALUES (?,?,?,?,?,?,?)";
            $STH = $this->DBH->prepare($query);

            $STH->bindParam(1,$this->cat_id);
            $STH->bindParam(2,$this->doctor_master_id);
            $STH->bindParam(3,$this->hospital_master_id);
            $STH->bindParam(4,$this->patient_limit);
            $STH->bindParam(5,$days[$count]);
            $STH->bindParam(6,$this->fees);
            $STH->bindParam(7,$this->time);
            
            $STH->execute();
            $count++;
        }
    }
    public function showDoctor(){
        $sql = "SELECT doctor_details.*, doctor_master.* FROM `doctor_details`,doctor_master WHERE doctor_master.id=doctor_details.doctor_master_id AND  doctor_master.status='1' AND doctor_details.`category_id`='$this->cat_id' AND doctor_details.`hospital_id`='$this->hospital_master_id' AND doctor_details.days='$this->appoint_day' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function showLimit(){
        $sql = "SELECT patient_limit FROM `doctor_details` WHERE `category_id`='$this->cat_id' AND `hospital_id`='$this->hos_id' AND doctor_master_id='$this->doc_id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }

    public function is_exist_email(){

        $query="SELECT * FROM `registration_info` WHERE `email`='$this->email'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function is_exist_user(){

        $query="SELECT * FROM `registration_info` WHERE `user_name`='$this->user_name'";
        $STH=$this->DBH->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function loginCheck(){
        $query = "SELECT * FROM `registration_info` WHERE `user_name`='$this->user_name' AND `password`='$this->password'";
        $STH=$this->DBH->query($query);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->fetchAll();


        $count = $STH->rowCount();
        if ($count > 0) {

            return TRUE;
        } else {
            return FALSE;
        }
    }


    public function log_out(){
        $_SESSION['u_name']="";
        return TRUE;
    }
    public function logged_in(){
        if ((array_key_exists('u_name', $_SESSION)) && (!empty($_SESSION['u_name']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}