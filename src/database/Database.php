<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 2/22/2017
 * Time: 4:41 PM
 */

namespace App\database;
use PDO, PDOException;

class Database
{
    public $DBH;
    public function __construct()
    {
        try{

            $this->DBH = new PDO('mysql:host=localhost;dbname=omas', "root", "");
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

            return $this->DBH;
        }
        catch(PDOException $error){
            echo "Database Error: ". $error->getMessage();
        }
    }
}
