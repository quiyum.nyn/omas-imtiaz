<?php
require_once ("../vendor/autoload.php");
use App\model\Temp;
use App\Message\Message;
use App\Utility\Utility;
$obj=new Temp();
if($_POST['category_id']=='reject'){
    Message::setMessage("Failed! Select a category please!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    $obj->prepareData($_POST);
    $catStatus=$obj->is_exist_category();
    if(!$catStatus){
        $obj->store();
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }
    else{
        Message::setMessage("Failed! This category is already selected!");
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }
}
