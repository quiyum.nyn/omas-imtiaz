<?php
require_once ("../vendor/autoload.php");
use App\model\Doctor_master;
use App\model\Doctor_details;
$detaiilsObj=new Doctor_details();
use App\Utility\Utility;
use App\Message\Message;

$object=new Doctor_master();
if(isset($_FILES['picture_data']['name']))
{
    $picName=time().$_FILES['picture_data']['name'];
    $tmp_name=$_FILES['picture_data']['tmp_name'];
     move_uploaded_file($tmp_name,'../resources/doctor_photos/'.$picName);
    $_POST['picture_name']=$picName;
}
$object->prepareData($_POST);
$object->store();
$_POST['days_details']=implode(",",$_POST['days']);
$detaiilsObj->prepareData($_POST);
$detaiilsObj->store();
Message::setMessage("Success! Registration successfully Completed! Login Yourself");
Utility::redirect('../views/login.php');


