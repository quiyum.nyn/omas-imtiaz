<?php
require_once ("../vendor/autoload.php");
use App\Utility\Utility;
use App\Message\Message;
use App\model\Doctor_master;
$obj=new Doctor_master();
$obj->prepareData($_GET);
$obj->activeDoctor();
Message::setMessage("Active! All doctor activities!");
Utility::redirect('../views/admin/doctor_list.php');
