<?php
require_once("../vendor/autoload.php");
use App\model\Doctor_master;
use App\model\Super_admins;
use App\Utility\Utility;
use App\Message\Message;
$auth=new Doctor_master();
$doctor= $auth->prepareData($_POST)->doctorLogin();
$doctorData=$auth->doctorId();
if($doctor){
    if($doctorData->status=='2'){
        $auth->reactiveDoctor();
        $_SESSION['role_status']=0;
        $_SESSION['email']=$_POST['email'];
        $_SESSION['doctor_id']=$doctorData->id;
        Utility::redirect('../views/doctor/index.php');
    }
    elseif ($doctorData->status=='0'){
        Message::setMessage("Your account has been suspend!");
        Utility::redirect('../views/login.php');
    }
    elseif ($doctorData->status=='1'){
        Message::setMessage("Please Login Again");
        Utility::redirect('../views/login.php');
    }
}
else{
    Message::setMessage("Email and password doesn't match! Try Again");
    Utility::redirect('../views/login.php');

}


?>