<?php
require_once ("../vendor/autoload.php");
use App\model\Emergency;
use App\Message\Message;
use App\Utility\Utility;
$obj=new Emergency();
$obj->prepareData($_POST);
$status=$obj->is_exist_category();
if($status){
    Message::setMessage("Reject! This Data is already stored!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    $obj->store();
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
