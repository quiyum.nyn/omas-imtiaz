<?php
require_once ("../vendor/autoload.php");
use App\model\Emergency;
use App\Utility\Utility;
use App\Message\Message;
$obj=new Emergency();
$obj->prepareData($_GET);
$obj->deleteOne();
Message::setMessage("Delete! Deleted specific data!");
return Utility::redirect($_SERVER['HTTP_REFERER']);