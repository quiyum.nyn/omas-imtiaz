<?php
require_once ("../vendor/autoload.php");
use App\model\Patients;
use App\model\Doctor_details;
use App\Message\Message;
use App\Utility\Utility;
$obj=new Patients();
$docObj=new Doctor_details();

if(isset($_FILES['picture']['name']))
{
    $picName=time().$_FILES['picture']['name'];
    $tmp_name=$_FILES['picture']['tmp_name'];
    move_uploaded_file($tmp_name,'../resources/patient_photos/'.$picName);
    $_POST['picture_name']=$picName;
}
$_POST['password']=uniqid();

$obj->prepareData($_POST);
$docObj->prepareData($_POST);
$obj->store();


