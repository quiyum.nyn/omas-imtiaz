<?php
require_once ("../vendor/autoload.php");
use App\Utility\Utility;
use App\Message\Message;
use App\model\Doctor_master;
$obj=new Doctor_master();
$obj->prepareData($_GET);
$obj->deactiveDoctor();
Message::setMessage("De-active Account! Login again to re-active your account");
Utility::redirect('../views/reactive.php');
