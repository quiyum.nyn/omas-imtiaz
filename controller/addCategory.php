<?php
require_once ("../vendor/autoload.php");
use App\model\Category;
use App\Message\Message;
use App\Utility\Utility;
$obj=new Category();
$obj->prepareData($_POST);
$status=$obj->is_exist_category();
if($status){
    Message::setMessage("Reject! This category is already stored!");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
else{
    $obj->store();
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}
