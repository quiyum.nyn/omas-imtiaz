-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 06, 2017 at 10:35 PM
-- Server version: 10.2.3-MariaDB-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `omas`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `status`) VALUES
(2, 'Anesthesiologist', 1),
(3, 'Cardiac Electrologists', 1),
(4, 'Cardiologists', 1),
(5, 'Child & Adolescent Psychiatrists', 1),
(6, 'Dermatologists', 1),
(7, 'Diagnostic Radiologists', 1),
(8, 'Family Medicine Doctor', 1),
(9, 'Hematologists', 1),
(10, 'Neurologists', 1),
(11, 'Nuclear Medicine Specialists', 1),
(12, 'Orthopedists', 1),
(13, 'Pathologists', 1),
(14, 'Pediatric Surgeons', 1),
(15, 'Urologists', 1),
(16, 'Plastic surgeons', 1),
(17, 'Cardiology', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_details`
--

CREATE TABLE `doctor_details` (
  `id` int(11) NOT NULL,
  `doctor_master_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `patient_limit` int(11) NOT NULL,
  `days` varchar(255) NOT NULL,
  `fees` int(11) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_details`
--

INSERT INTO `doctor_details` (`id`, `doctor_master_id`, `category_id`, `hospital_id`, `patient_limit`, `days`, `fees`, `time`) VALUES
(34, 34, 6, 34, 1, 'Sat', 345, '10:00am to 01:00pm'),
(35, 34, 6, 34, 2, 'Sun', 345, '10:00am to 01:00pm'),
(36, 34, 6, 34, 1, 'Mon', 345, '10:00am to 01:00pm'),
(37, 35, 6, 34, 23, 'Sun', 600, '10:00am to 01:00pm'),
(38, 35, 6, 34, 23, 'Mon', 600, '10:00am to 01:00pm'),
(39, 35, 6, 34, 23, 'Wed', 600, '10:00am to 01:00pm'),
(40, 35, 6, 34, 23, 'Thu', 600, '10:00am to 01:00pm'),
(41, 36, 6, 34, 25, 'Sat', 700, '10:00am to 01:00pm'),
(42, 36, 6, 34, 25, 'Mon', 700, '10:00am to 01:00pm'),
(43, 36, 6, 34, 25, 'Wed', 700, '10:00am to 01:00pm'),
(44, 37, 6, 34, 30, 'Sat', 1000, '3:00pm to 9:00pm'),
(45, 37, 6, 34, 30, 'Sun', 1000, '3:00pm to 9:00pm'),
(46, 37, 6, 34, 30, 'Mon', 1000, '3:00pm to 9:00pm'),
(47, 37, 6, 34, 30, 'Tue', 1000, '3:00pm to 9:00pm'),
(48, 37, 6, 34, 30, 'Wed', 1000, '3:00pm to 9:00pm'),
(49, 37, 6, 34, 30, 'Thu', 1000, '3:00pm to 9:00pm'),
(50, 37, 6, 34, 30, 'Fri', 1000, '3:00pm to 9:00pm');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_master`
--

CREATE TABLE `doctor_master` (
  `id` int(11) NOT NULL,
  `doctor_name` varchar(255) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `degree` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `reg_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_master`
--

INSERT INTO `doctor_master` (`id`, `doctor_name`, `contact`, `gender`, `degree`, `password`, `email`, `picture`, `status`, `reg_date`) VALUES
(34, 'xxcvxdfcdxz', '8678676', 'Male', 'dfdsf', 'e10adc3949ba59abbe56e057f20f883e', 'adsas@fgfd.c', '1503618264adil.jpg', 1, '2017-09-06 22:18:43'),
(35, 'xdfsxfds', '4521231', 'Male', 'dsfsdfds', 'e10adc3949ba59abbe56e057f20f883e', 'qwqw@gm.com', '1503618351ayub.jpg', 1, '2017-09-06 21:30:46'),
(36, 'Arif Hasan', '01826132308', 'Male', 'MBBS, FCPS', 'e10adc3949ba59abbe56e057f20f883e', 'a@gmail.com', '1503674713a j m nasir.jpg', 1, '2017-09-06 22:20:44'),
(37, 'Imtiaz', '01826132308', 'Male', 'MBBS, FCPS(thiland),MSBS(india),Intern doctor in CMC', 'fcea920f7412b5da7be0cf42b8c93759', 'b@gmail.com', '15047366301-8-2017 10-31-08 PM_0489.jpg', 1, '2017-09-06 22:23:50');

-- --------------------------------------------------------

--
-- Table structure for table `emergency`
--

CREATE TABLE `emergency` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `type` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emergency`
--

INSERT INTO `emergency` (`id`, `name`, `phone`, `type`) VALUES
(1, 'Mohammad Rafiq', '0123456', 'Doctor');

-- --------------------------------------------------------

--
-- Table structure for table `hospital_details`
--

CREATE TABLE `hospital_details` (
  `id` int(11) NOT NULL,
  `master_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospital_details`
--

INSERT INTO `hospital_details` (`id`, `master_id`, `category_id`) VALUES
(16, 31, 17),
(17, 31, 16),
(18, 31, 15),
(19, 31, 14),
(20, 31, 10),
(21, 31, 9),
(22, 32, 17),
(23, 32, 7),
(24, 32, 2),
(25, 32, 5),
(26, 32, 12),
(27, 33, 15),
(28, 33, 6),
(29, 33, 9),
(30, 33, 10),
(31, 33, 11),
(32, 34, 17),
(33, 34, 6),
(34, 34, 8),
(35, 34, 11),
(36, 34, 12),
(37, 35, 16),
(38, 35, 11),
(39, 35, 4),
(40, 35, 3),
(41, 35, 2);

-- --------------------------------------------------------

--
-- Table structure for table `hospital_master`
--

CREATE TABLE `hospital_master` (
  `id` int(11) NOT NULL,
  `hospital_name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `contact` varchar(22) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospital_master`
--

INSERT INTO `hospital_master` (`id`, `hospital_name`, `location`, `contact`, `status`) VALUES
(31, 'Max Hospital LTD', 'Mehedibagh', '0182926372', 1),
(32, 'Chittagong Metro-politon Hospital', 'GEC Circle', '0192737212', 1),
(33, 'CSCR', 'Probortok Circle', '0192638723', 1),
(34, 'National Hospital LTD', 'Mehedibagh', '019236272', 1),
(35, 'Chittagong Medical College Hospital', 'Probortok Circle', '018100000000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `patient_name` varchar(255) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `hospital_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `booking_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `appoint_date` date NOT NULL,
  `serial` int(11) NOT NULL DEFAULT 0,
  `picture` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `patient_name`, `contact`, `gender`, `age`, `email`, `password`, `hospital_id`, `category_id`, `doctor_id`, `booking_date`, `appoint_date`, `serial`, `picture`, `time`) VALUES
(50023, 'dfdfd', '35353', 'Male', 12, 'a@g.c', '599f74cd9fccd', 34, 6, 34, '2017-08-25 15:51:17', '2017-08-27', 1, '1503622349ayub.jpg', '10:00am to 01:00pm'),
(50024, 'bihciosah', '65273728', 'Male', 12, 'a@g.c', '599f76552da2f', 34, 6, 35, '2017-08-25 15:51:14', '2017-08-27', 1, '1503622741a j m nasir.jpg', '10:00am to 01:00pm'),
(50025, 'sdfdfd', '86786', 'Male', 23, 'a@g.c', '599f7c4d0c0d1', 34, 6, 34, '2017-08-25 15:51:10', '2017-08-27', 2, '1503624269isa .jpg', '10:00am to 01:00pm'),
(50026, 'Irfan', '0182345678', 'Male', 24, 'irfan@gmail.com', '59a0476066880', 34, 6, 34, '2017-08-25 15:50:56', '2017-08-26', 1, '1503676256isa .jpg', '10:00am to 01:00pm'),
(50027, 'Abir', '01812111111', 'Male', 23, 'a@g.c', '59a05c8504c4e', 34, 6, 36, '2017-08-25 17:21:09', '2017-08-25', 1, '150368166920507003_1989761374635752_1638441164394032633_o (1).jpg', '10:00am to 01:00pm'),
(50028, 'Abir', '01826132308', 'Male', 24, 'a@g.c', '59a07f81c534c', 34, 6, 36, '2017-08-25 19:50:25', '2017-08-26', 1, '1503690625ayub.jpg', '10:00am to 01:00pm'),
(50029, 'Hasan', '0183400000', 'Male', 25, 'a@g.c', '59a07fa18e0d9', 34, 6, 36, '2017-08-25 19:50:57', '2017-08-26', 2, '1503690657adil jr.jpg', '10:00am to 01:00pm');

-- --------------------------------------------------------

--
-- Table structure for table `super_admin`
--

CREATE TABLE `super_admin` (
  `id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_contact` int(11) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_picture` varchar(255) NOT NULL DEFAULT 'admin.jpg',
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `super_admin`
--

INSERT INTO `super_admin` (`id`, `admin_name`, `admin_email`, `admin_contact`, `admin_password`, `admin_picture`, `date`) VALUES
(1, 'Imtiaj', 'imtiaj@omas.com', 1821542846, 'e10adc3949ba59abbe56e057f20f883e', 'admin.jpg', '2017-08-10 15:44:28');

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE `temp` (
  `id` int(11) NOT NULL,
  `hospital_name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_details`
--
ALTER TABLE `doctor_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_master`
--
ALTER TABLE `doctor_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emergency`
--
ALTER TABLE `emergency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospital_details`
--
ALTER TABLE `hospital_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospital_master`
--
ALTER TABLE `hospital_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `super_admin`
--
ALTER TABLE `super_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp`
--
ALTER TABLE `temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `doctor_details`
--
ALTER TABLE `doctor_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `doctor_master`
--
ALTER TABLE `doctor_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `emergency`
--
ALTER TABLE `emergency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hospital_details`
--
ALTER TABLE `hospital_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `hospital_master`
--
ALTER TABLE `hospital_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50030;
--
-- AUTO_INCREMENT for table `super_admin`
--
ALTER TABLE `super_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `temp`
--
ALTER TABLE `temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
